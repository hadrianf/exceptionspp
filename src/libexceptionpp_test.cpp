/*
Libexceptionpp
Copyright (C) 2019-2020 - Hadrian Ferran
*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>

#include "../include/libexceptionpp.hpp"
#include "../test/catch.hpp"

TEST_CASE("Custom Exceptions") {
  bool Exception_Caught = false;
  SECTION("Basic Error Code") {
    Exceptionpp Test_Exception(1);
    try {
      throw Test_Exception;
    } catch (Exceptionpp &e) {
      Exception_Caught = true;
      CHECK(strcmp(e.what(), (EXCEPTIONPP)) == 0);
      CHECK(e.what_Explanation().compare(ERR_UNDEFINED) == 0);
      CHECK(e.what_Err() == 1);
      CHECK(e.what_Line() == UINT64_MAX);
      CHECK(e.what_File().compare("") == 0);
      CHECK(e.what_Extensive_Explanation().compare("") == 0);
    }
    CHECK(Exception_Caught);
  }

  SECTION("Basic Error Code and message") {
    Exceptionpp Test_Exception(1, "Just an exception");
    try {
      throw Test_Exception;
    } catch (Exceptionpp &e) {
      Exception_Caught = true;
      CHECK(strcmp(e.what(), (EXCEPTIONPP)) == 0);
      CHECK(e.what_Explanation().compare("Just an exception") == 0);
      CHECK(e.what_Err() == 1);
      CHECK(e.what_Line() == UINT64_MAX);
      CHECK(e.what_File().compare("") == 0);
      CHECK(e.what_Extensive_Explanation().compare("") == 0);
    }
    CHECK(Exception_Caught);
  }

  SECTION("Basic Error Code, message, and extensive message") {
    Exceptionpp Test_Exception(1, "Just an exception", "Really though");
    try {
      throw Test_Exception;
    } catch (Exceptionpp &e) {
      Exception_Caught = true;
      CHECK(strcmp(e.what(), (EXCEPTIONPP)) == 0);
      CHECK(e.what_Explanation().compare("Just an exception") == 0);
      CHECK(e.what_Err() == 1);
      CHECK(e.what_Line() == UINT64_MAX);
      CHECK(e.what_File().compare("") == 0);
      CHECK(e.what_Extensive_Explanation().compare("Really though") == 0);
    }
    CHECK(Exception_Caught);
  }
  SECTION("Full exception") {
    uint64_t Test_Line = __LINE__ + 2;
    Exceptionpp Test_Exception(1, "Just an exception", "Really though",
                               __LINE__, __FILE__);
    try {
      throw Test_Exception;
    } catch (Exceptionpp &e) {
      Exception_Caught = true;
      CHECK(strcmp(e.what(), (EXCEPTIONPP)) == 0);
      CHECK(e.what_Explanation().compare("Just an exception") == 0);
      CHECK(e.what_Err() == 1);
      CHECK(e.what_Line() == Test_Line);
      CHECK(e.what_File().compare(__FILE__) == 0);
      CHECK(e.what_Extensive_Explanation().compare("Really though") == 0);
      Output_Exceptionpp(e);
    } catch (std::exception &e) {
      Exception_Caught = false;
    }
    CHECK(Exception_Caught);
  }
}