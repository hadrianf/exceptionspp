/*
Libexceptionpp
Copyright (C) 2019-2020 - Hadrian Ferran
*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <cstdint>
#include <exception>
#include <sstream>
#include <string>

#define EXCEPTIONPP "EXCEPTIONPP"
#define ERR_UNDEFINED "Error type undefined"

/*
    In case of undefined Exceptionpp error messages, the default values are:
    Line = UINT64_MAX
    Err = INT64_MAX
    File = ""
    Explanation = "Error type undefined"
    Extensive_Explanation = ""
*/
class Exceptionpp : public std::exception {
public:
  Exceptionpp();
  Exceptionpp(int64_t New_Err);
  Exceptionpp(int64_t New_Err, uint64_t New_Line, std::string New_File);
  Exceptionpp(int64_t New_Err, std::string New_Explanation);
  Exceptionpp(int64_t New_Err, std::string New_Explanation, uint64_t New_Line,
              std::string New_File);
  Exceptionpp(int64_t New_Err, std::string New_Explanation,
              std::string New_Extensive_Explanation);
  Exceptionpp(int64_t New_Err, std::string New_Explanation,
              std::string New_Extensive_Explanation, uint64_t New_Line,
              std::string New_File);
  ~Exceptionpp();
  const char *what() const noexcept;
  std::string what_Explanation() const noexcept;
  int64_t what_Err() const noexcept;
  uint64_t what_Line() const noexcept;
  std::string what_File() const noexcept;
  std::string what_Extensive_Explanation() const noexcept;
  std::string Get_Output() const noexcept;
  void Get_Output(std::stringstream &Retval) const noexcept;
  void Print_Output() noexcept;
  bool Was_Printed() const noexcept;

protected:
  int64_t Err = INT64_MAX;
  uint64_t Line = UINT64_MAX;
  bool Printed = false;
  std::string File;
  std::string What = EXCEPTIONPP;
  std::string Explanation;
  std::string Extensive_Explanation;
};
