#!/bin/bash
installopt=false
removeopt=false
debug=false
makeclean=false
genericbuild=false

while getopts ":idrcg" option; do
    case $option in
        i) installopt=true;;
        d) debug=true;;
        r) removeopt=true;;
		c) makeclean=true;;
		g) genericbuild=true;;
    esac
done

if [ "$removeopt" = true ]
	then
		echo "removing files"
		cat install_manifest.txt |exec sudo xargs rm && exec sudo ldconfig
		exec sudo rm install_manifest.txt
		exit 0
fi


if [ -f *.so ]
then
	rm *.so
fi

if [ -f CMakeCache.txt ]
then
	rm -f CMakeCache.txt
fi

if [ "$debug" = true ] 
then
	cmake -DCMAKE_BUILD_TYPE=Debug -DTARGET_GROUP:STRING=library .
else
	if [ "$genericbuild" = true ]
	then
		echo "Building generic build."
		cmake -DCMAKE_BUILD_TYPE=Release -DTARGET_GROUP:STRING=library -DGB=ON .
	else
		echo "Building machine-specific build."
		cmake -DCMAKE_BUILD_TYPE=Release -DTARGET_GROUP:STRING=library -DGB=OFF .
	fi
fi

if [ "$makeclean" = true ]
then
	make clean
	exit 0
fi

echo "Building with" $(nproc) "threads."
make -j$(nproc)

if [ -f *.so ] && [ "$installopt" = true  ] 
then
    exec sudo make install && exec sudo ldconfig
fi

exit 0
