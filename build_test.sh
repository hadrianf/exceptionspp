#!/bin/bash


if [ -f LIBEXCEPTIONPPTEST ]
then
	rm LIBEXCEPTIONPPTEST
fi
cmake -DCMAKE_BUILD_TYPE=Debug -DTARGET_GROUP:STRING=test .
echo "Building with" $(nproc) "threads."
make -j$(nproc)
if [ -f testLIBEXCEPTIONPPTEST ]
then
	mv testLIBEXCEPTIONPPTEST LIBEXCEPTIONPPTEST
fi

if [ -f LIBEXCEPTIONPPTEST ]
then
	chmod +x LIBEXCEPTIONPPTEST
	./LIBEXCEPTIONPPTEST -s --use-colour yes
fi
exit 0
