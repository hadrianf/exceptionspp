/*
Libexceptionpp
Copyright (C) 2019-2020 - Hadrian Ferran
*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/libexceptionpp.hpp"
#include <cstring>
#include <iostream>
#include <sstream>

Exceptionpp::Exceptionpp() {}

Exceptionpp::Exceptionpp(int64_t New_Err)
    : Exceptionpp(New_Err, ERR_UNDEFINED, "", UINT64_MAX, ""){};

Exceptionpp::Exceptionpp(int64_t New_Err, uint64_t New_Line,
                         std::string New_File)
    : Exceptionpp(New_Err, ERR_UNDEFINED, "", New_Line, New_File){};

Exceptionpp::Exceptionpp(int64_t New_Err, std::string New_Explanation)
    : Exceptionpp(New_Err, New_Explanation, "", UINT64_MAX, ""){};

Exceptionpp::Exceptionpp(int64_t New_Err, std::string New_Explanation,
                         uint64_t New_Line, std::string New_File)
    : Exceptionpp(New_Err, New_Explanation, "", New_Line, New_File){};

Exceptionpp::Exceptionpp(int64_t New_Err, std::string New_Explanation,
                         std::string New_Extensive_Explanation)
    : Exceptionpp(New_Err, New_Explanation, New_Extensive_Explanation,
                  UINT64_MAX, ""){};

Exceptionpp::Exceptionpp(int64_t New_Err, std::string New_Explanation,
                         std::string New_Extensive_Explanation,
                         uint64_t New_Line, std::string New_File)
    : Err(New_Err), Explanation(New_Explanation),
      Extensive_Explanation(New_Extensive_Explanation), Line(New_Line),
      File(New_File) {}

Exceptionpp::~Exceptionpp() { return; }

const char *Exceptionpp::what() const noexcept { return What.c_str(); }

std::string Exceptionpp::what_Explanation() const noexcept {
  return Explanation;
}

int64_t Exceptionpp::what_Err() const noexcept { return Err; }

uint64_t Exceptionpp::what_Line() const noexcept { return Line; }

std::string Exceptionpp::what_File() const noexcept { return File; }

std::string Exceptionpp::what_Extensive_Explanation() const noexcept {
  return Extensive_Explanation;
}

std::string Exceptionpp::Get_Output() const noexcept {
  std::stringstream Output_Message;
  Get_Output(Output_Message);
  return Output_Message.str();
}

void Exceptionpp::Get_Output(std::stringstream &Retval) const noexcept {
  bool Any_Message_Outputted = false;
  if (what_Err() != INT64_MAX) {
    Any_Message_Outputted = true;
    Retval << "Error code: " << what_Err();
  }
  if (what_Explanation().compare(ERR_UNDEFINED) != 0) {
    Any_Message_Outputted = true;
    Retval << std::endl << "Error message: " << what_Explanation();
  }
  if (what_Extensive_Explanation().compare("") != 0) {
    Retval << std::endl
           << "Extensive explanation: " << what_Extensive_Explanation();
  }
  if ((what_File().compare("") != 0) && (what_Line() != UINT64_MAX)) {
    Retval << std::endl << "In file: " << what_File() << ":" << what_Line();
  }
  if (!Any_Message_Outputted) {
    Retval << "Unknown error, no information present.";
  }
}
void Exceptionpp::Print_Output() noexcept {
  Printed = true;
  std::cerr << Get_Output();
}

bool Exceptionpp::Was_Printed() const noexcept { return Printed; }
